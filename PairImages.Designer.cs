﻿namespace WindowsFormsExam
{
    partial class PairImages
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(PairImages));
            this.titleBlinking = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel = new System.Windows.Forms.TableLayoutPanel();
            this.labelBox16 = new System.Windows.Forms.Label();
            this.labelBox15 = new System.Windows.Forms.Label();
            this.labelBox14 = new System.Windows.Forms.Label();
            this.labelBox13 = new System.Windows.Forms.Label();
            this.labelBox12 = new System.Windows.Forms.Label();
            this.labelBox11 = new System.Windows.Forms.Label();
            this.labelBox10 = new System.Windows.Forms.Label();
            this.labelBox9 = new System.Windows.Forms.Label();
            this.labelBox8 = new System.Windows.Forms.Label();
            this.labelBox7 = new System.Windows.Forms.Label();
            this.labelBox6 = new System.Windows.Forms.Label();
            this.labelBox5 = new System.Windows.Forms.Label();
            this.labelBox4 = new System.Windows.Forms.Label();
            this.labelBox3 = new System.Windows.Forms.Label();
            this.labelBox2 = new System.Windows.Forms.Label();
            this.labelBox1 = new System.Windows.Forms.Label();
            this.timerIcons = new System.Windows.Forms.Timer(this.components);
            this.tableLayoutPanel.SuspendLayout();
            this.SuspendLayout();
            // 
            // titleBlinking
            // 
            this.titleBlinking.Tick += new System.EventHandler(this.TitleBlinking_Tick);
            // 
            // tableLayoutPanel
            // 
            this.tableLayoutPanel.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel.CellBorderStyle = System.Windows.Forms.TableLayoutPanelCellBorderStyle.Outset;
            this.tableLayoutPanel.ColumnCount = 4;
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.Controls.Add(this.labelBox16, 3, 3);
            this.tableLayoutPanel.Controls.Add(this.labelBox15, 2, 3);
            this.tableLayoutPanel.Controls.Add(this.labelBox14, 1, 3);
            this.tableLayoutPanel.Controls.Add(this.labelBox13, 0, 3);
            this.tableLayoutPanel.Controls.Add(this.labelBox12, 3, 2);
            this.tableLayoutPanel.Controls.Add(this.labelBox11, 2, 2);
            this.tableLayoutPanel.Controls.Add(this.labelBox10, 1, 2);
            this.tableLayoutPanel.Controls.Add(this.labelBox9, 0, 2);
            this.tableLayoutPanel.Controls.Add(this.labelBox8, 3, 1);
            this.tableLayoutPanel.Controls.Add(this.labelBox7, 2, 1);
            this.tableLayoutPanel.Controls.Add(this.labelBox6, 1, 1);
            this.tableLayoutPanel.Controls.Add(this.labelBox5, 0, 1);
            this.tableLayoutPanel.Controls.Add(this.labelBox4, 3, 0);
            this.tableLayoutPanel.Controls.Add(this.labelBox3, 2, 0);
            this.tableLayoutPanel.Controls.Add(this.labelBox2, 1, 0);
            this.tableLayoutPanel.Controls.Add(this.labelBox1, 0, 0);
            this.tableLayoutPanel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel.Name = "tableLayoutPanel";
            this.tableLayoutPanel.RowCount = 4;
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel.Size = new System.Drawing.Size(484, 461);
            this.tableLayoutPanel.TabIndex = 0;
            // 
            // labelBox16
            // 
            this.labelBox16.BackColor = System.Drawing.Color.SteelBlue;
            this.labelBox16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBox16.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelBox16.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelBox16.Location = new System.Drawing.Point(362, 344);
            this.labelBox16.Margin = new System.Windows.Forms.Padding(0);
            this.labelBox16.Name = "labelBox16";
            this.labelBox16.Size = new System.Drawing.Size(120, 115);
            this.labelBox16.TabIndex = 15;
            this.labelBox16.Text = "c";
            this.labelBox16.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelBox16.Click += new System.EventHandler(this.label_Click);
            // 
            // labelBox15
            // 
            this.labelBox15.BackColor = System.Drawing.Color.SteelBlue;
            this.labelBox15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBox15.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelBox15.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelBox15.Location = new System.Drawing.Point(242, 344);
            this.labelBox15.Margin = new System.Windows.Forms.Padding(0);
            this.labelBox15.Name = "labelBox15";
            this.labelBox15.Size = new System.Drawing.Size(118, 115);
            this.labelBox15.TabIndex = 14;
            this.labelBox15.Text = "c";
            this.labelBox15.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelBox15.Click += new System.EventHandler(this.label_Click);
            // 
            // labelBox14
            // 
            this.labelBox14.BackColor = System.Drawing.Color.SteelBlue;
            this.labelBox14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBox14.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelBox14.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelBox14.Location = new System.Drawing.Point(122, 344);
            this.labelBox14.Margin = new System.Windows.Forms.Padding(0);
            this.labelBox14.Name = "labelBox14";
            this.labelBox14.Size = new System.Drawing.Size(118, 115);
            this.labelBox14.TabIndex = 13;
            this.labelBox14.Text = "c";
            this.labelBox14.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelBox14.Click += new System.EventHandler(this.label_Click);
            // 
            // labelBox13
            // 
            this.labelBox13.BackColor = System.Drawing.Color.SteelBlue;
            this.labelBox13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBox13.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelBox13.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelBox13.Location = new System.Drawing.Point(2, 344);
            this.labelBox13.Margin = new System.Windows.Forms.Padding(0);
            this.labelBox13.Name = "labelBox13";
            this.labelBox13.Size = new System.Drawing.Size(118, 115);
            this.labelBox13.TabIndex = 12;
            this.labelBox13.Text = "c";
            this.labelBox13.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelBox13.Click += new System.EventHandler(this.label_Click);
            // 
            // labelBox12
            // 
            this.labelBox12.BackColor = System.Drawing.Color.SteelBlue;
            this.labelBox12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBox12.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelBox12.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelBox12.Location = new System.Drawing.Point(362, 230);
            this.labelBox12.Margin = new System.Windows.Forms.Padding(0);
            this.labelBox12.Name = "labelBox12";
            this.labelBox12.Size = new System.Drawing.Size(120, 112);
            this.labelBox12.TabIndex = 11;
            this.labelBox12.Text = "c";
            this.labelBox12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelBox12.Click += new System.EventHandler(this.label_Click);
            // 
            // labelBox11
            // 
            this.labelBox11.BackColor = System.Drawing.Color.SteelBlue;
            this.labelBox11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBox11.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelBox11.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelBox11.Location = new System.Drawing.Point(242, 230);
            this.labelBox11.Margin = new System.Windows.Forms.Padding(0);
            this.labelBox11.Name = "labelBox11";
            this.labelBox11.Size = new System.Drawing.Size(118, 112);
            this.labelBox11.TabIndex = 10;
            this.labelBox11.Text = "c";
            this.labelBox11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelBox11.Click += new System.EventHandler(this.label_Click);
            // 
            // labelBox10
            // 
            this.labelBox10.BackColor = System.Drawing.Color.SteelBlue;
            this.labelBox10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBox10.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelBox10.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelBox10.Location = new System.Drawing.Point(122, 230);
            this.labelBox10.Margin = new System.Windows.Forms.Padding(0);
            this.labelBox10.Name = "labelBox10";
            this.labelBox10.Size = new System.Drawing.Size(118, 112);
            this.labelBox10.TabIndex = 9;
            this.labelBox10.Text = "c";
            this.labelBox10.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelBox10.Click += new System.EventHandler(this.label_Click);
            // 
            // labelBox9
            // 
            this.labelBox9.BackColor = System.Drawing.Color.SteelBlue;
            this.labelBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBox9.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelBox9.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelBox9.Location = new System.Drawing.Point(2, 230);
            this.labelBox9.Margin = new System.Windows.Forms.Padding(0);
            this.labelBox9.Name = "labelBox9";
            this.labelBox9.Size = new System.Drawing.Size(118, 112);
            this.labelBox9.TabIndex = 8;
            this.labelBox9.Text = "c";
            this.labelBox9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelBox9.Click += new System.EventHandler(this.label_Click);
            // 
            // labelBox8
            // 
            this.labelBox8.BackColor = System.Drawing.Color.SteelBlue;
            this.labelBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBox8.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelBox8.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelBox8.Location = new System.Drawing.Point(362, 116);
            this.labelBox8.Margin = new System.Windows.Forms.Padding(0);
            this.labelBox8.Name = "labelBox8";
            this.labelBox8.Size = new System.Drawing.Size(120, 112);
            this.labelBox8.TabIndex = 7;
            this.labelBox8.Text = "c";
            this.labelBox8.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelBox8.Click += new System.EventHandler(this.label_Click);
            // 
            // labelBox7
            // 
            this.labelBox7.BackColor = System.Drawing.Color.SteelBlue;
            this.labelBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBox7.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelBox7.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelBox7.Location = new System.Drawing.Point(242, 116);
            this.labelBox7.Margin = new System.Windows.Forms.Padding(0);
            this.labelBox7.Name = "labelBox7";
            this.labelBox7.Size = new System.Drawing.Size(118, 112);
            this.labelBox7.TabIndex = 6;
            this.labelBox7.Text = "c";
            this.labelBox7.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelBox7.Click += new System.EventHandler(this.label_Click);
            // 
            // labelBox6
            // 
            this.labelBox6.BackColor = System.Drawing.Color.SteelBlue;
            this.labelBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBox6.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelBox6.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelBox6.Location = new System.Drawing.Point(122, 116);
            this.labelBox6.Margin = new System.Windows.Forms.Padding(0);
            this.labelBox6.Name = "labelBox6";
            this.labelBox6.Size = new System.Drawing.Size(118, 112);
            this.labelBox6.TabIndex = 5;
            this.labelBox6.Text = "c";
            this.labelBox6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelBox6.Click += new System.EventHandler(this.label_Click);
            // 
            // labelBox5
            // 
            this.labelBox5.BackColor = System.Drawing.Color.SteelBlue;
            this.labelBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBox5.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelBox5.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelBox5.Location = new System.Drawing.Point(2, 116);
            this.labelBox5.Margin = new System.Windows.Forms.Padding(0);
            this.labelBox5.Name = "labelBox5";
            this.labelBox5.Size = new System.Drawing.Size(118, 112);
            this.labelBox5.TabIndex = 4;
            this.labelBox5.Text = "c";
            this.labelBox5.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelBox5.Click += new System.EventHandler(this.label_Click);
            // 
            // labelBox4
            // 
            this.labelBox4.BackColor = System.Drawing.Color.SteelBlue;
            this.labelBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBox4.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelBox4.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelBox4.Location = new System.Drawing.Point(362, 2);
            this.labelBox4.Margin = new System.Windows.Forms.Padding(0);
            this.labelBox4.Name = "labelBox4";
            this.labelBox4.Size = new System.Drawing.Size(120, 112);
            this.labelBox4.TabIndex = 3;
            this.labelBox4.Text = "c";
            this.labelBox4.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelBox4.Click += new System.EventHandler(this.label_Click);
            // 
            // labelBox3
            // 
            this.labelBox3.BackColor = System.Drawing.Color.SteelBlue;
            this.labelBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBox3.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelBox3.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelBox3.Location = new System.Drawing.Point(242, 2);
            this.labelBox3.Margin = new System.Windows.Forms.Padding(0);
            this.labelBox3.Name = "labelBox3";
            this.labelBox3.Size = new System.Drawing.Size(118, 112);
            this.labelBox3.TabIndex = 2;
            this.labelBox3.Text = "c";
            this.labelBox3.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelBox3.Click += new System.EventHandler(this.label_Click);
            // 
            // labelBox2
            // 
            this.labelBox2.BackColor = System.Drawing.Color.SteelBlue;
            this.labelBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBox2.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelBox2.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelBox2.Location = new System.Drawing.Point(122, 2);
            this.labelBox2.Margin = new System.Windows.Forms.Padding(0);
            this.labelBox2.Name = "labelBox2";
            this.labelBox2.Size = new System.Drawing.Size(118, 112);
            this.labelBox2.TabIndex = 1;
            this.labelBox2.Text = "c";
            this.labelBox2.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelBox2.Click += new System.EventHandler(this.label_Click);
            // 
            // labelBox1
            // 
            this.labelBox1.BackColor = System.Drawing.Color.SteelBlue;
            this.labelBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.labelBox1.Font = new System.Drawing.Font("Webdings", 72F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(2)));
            this.labelBox1.ForeColor = System.Drawing.Color.SteelBlue;
            this.labelBox1.Location = new System.Drawing.Point(2, 2);
            this.labelBox1.Margin = new System.Windows.Forms.Padding(0);
            this.labelBox1.Name = "labelBox1";
            this.labelBox1.Size = new System.Drawing.Size(118, 112);
            this.labelBox1.TabIndex = 0;
            this.labelBox1.Text = "c";
            this.labelBox1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelBox1.Click += new System.EventHandler(this.label_Click);
            // 
            // timerIcons
            // 
            this.timerIcons.Interval = 750;
            this.timerIcons.Tick += new System.EventHandler(this.TimerIcons_Tick);
            // 
            // PairImages
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.ClientSize = new System.Drawing.Size(484, 461);
            this.Controls.Add(this.tableLayoutPanel);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.MaximizeBox = false;
            this.Name = "PairImages";
            this.Text = "Pair images";
            this.Load += new System.EventHandler(this.PairImages_Load);
            this.tableLayoutPanel.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer titleBlinking;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel;
        private System.Windows.Forms.Label labelBox1;
        private System.Windows.Forms.Label labelBox16;
        private System.Windows.Forms.Label labelBox15;
        private System.Windows.Forms.Label labelBox14;
        private System.Windows.Forms.Label labelBox13;
        private System.Windows.Forms.Label labelBox12;
        private System.Windows.Forms.Label labelBox11;
        private System.Windows.Forms.Label labelBox10;
        private System.Windows.Forms.Label labelBox9;
        private System.Windows.Forms.Label labelBox8;
        private System.Windows.Forms.Label labelBox7;
        private System.Windows.Forms.Label labelBox6;
        private System.Windows.Forms.Label labelBox5;
        private System.Windows.Forms.Label labelBox4;
        private System.Windows.Forms.Label labelBox3;
        private System.Windows.Forms.Label labelBox2;
        private System.Windows.Forms.Timer timerIcons;
    }
}

