﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Net;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsExam
{
    public partial class PairImages : Form
    {
        // List of image adresses
        private List<string> _imageAdresses;
        // List of images
        private List<Image> _imageList;
        // Randomize anything
        Random random;
        // List of label icons
        private List<string> _labelIcons;
        // Number of tries
        private ushort _triesNumber;
        // Labels to match
        private Label _firstMatch;
        private Label _secondMatch;

        public PairImages()
        {
            InitializeComponent();

            // Set adresses of images
            _imageAdresses = new List<string>()
            {
                "https://i.imgur.com/rIuaM6I.png",
                "https://i.imgur.com/XuXYxiy.png",
                "https://i.imgur.com/81zQR7V.png",
                "https://i.imgur.com/ILVm08m.png",
                "https://i.imgur.com/Cag0xSN.png",
                "https://i.imgur.com/O5BvmsD.png",
                "https://i.imgur.com/IasXszX.png",
                "https://i.imgur.com/z4ZKbKf.png",
                "https://i.imgur.com/dKetask.png",
                "https://i.imgur.com/jPLSa5F.png"
            };

            // Set size of from depends on...
            int width = Screen.PrimaryScreen.Bounds.Height / 2;
            int height = Screen.PrimaryScreen.Bounds.Height / 2;
            this.Size = new Size(width, height);

            // Set list of images
            _imageList = new List<Image>(10);
            foreach (string img in _imageAdresses)
            {
                WebRequest request = WebRequest.Create(img);
                WebResponse response = request.GetResponse();
                var stream = response.GetResponseStream();
                Bitmap bmp = new Bitmap(stream);
                stream.Dispose();
                _imageList.Add(bmp);
            }

            random = new Random();

            // Set label icons
            _labelIcons = new List<string>()
            {
                "!", "!", "N", "N", "Y", "Y", "k", "k", "d", "d", "l", "l", "\"", "\"", "Z", "Z"
            };

            _triesNumber = 0;
        }

        private async void PairImages_Load(object sender, EventArgs e)
        {
            // Set random background image
            this.BackgroundImage = _imageList[random.Next(_imageList.Count)];

            // Set icons in labels
            foreach (Control control in tableLayoutPanel.Controls)
            {
                Label labelIcon = control as Label;
                if (labelIcon != null)
                {
                    int randomNumber = random.Next(_labelIcons.Count);
                    labelIcon.Text = _labelIcons[randomNumber];
                    _labelIcons.RemoveAt(randomNumber);
                }
            }
            /*
            for (int i = 0; i < tableLayoutPanel.Controls.Count; i++)
            {
                Label labelIcon;
                if (tableLayoutPanel.Controls[i] is Label)
                {
                    labelIcon = (Label)tableLayoutPanel.Controls[i];
                }
                else
                {
                    continue;
                }
                int randomNumber = random.Next(0, _labelIcons.Count);
                labelIcon.Text = _labelIcons[randomNumber];
                _labelIcons.RemoveAt(randomNumber);
            }
            */

            // Setting text blink on form title for 3s
            titleBlinking.Start();
            await Task.Delay(3000);
            titleBlinking.Dispose();

            this.Text = "Pair images";
        }

        private void label_Click(object sender, EventArgs e)
        {
            if (_firstMatch != null && _secondMatch != null)
            {
                return;
            }

            // Set labels forecolors on click
            Label current = (Label)sender;
            if (current == null)
            {
                return;
            }
            if (current.ForeColor.Equals(Color.LightCyan))
            {
                return;
            }
            if (_firstMatch == null)
            {
                _firstMatch = current;
                _firstMatch.ForeColor = Color.LightCyan;
                return;
            }
            _secondMatch = current;
            _secondMatch.ForeColor = Color.LightCyan;

            // If labels text match - set them transparent
            if (_firstMatch.Text == _secondMatch.Text)
            {
                for (int i = 0; i < tableLayoutPanel.ColumnCount; i++)
                {
                    for (int j = 0; j < tableLayoutPanel.RowCount; j++)
                    {
                        if (tableLayoutPanel.GetControlFromPosition(i, j).Text == _firstMatch.Text)
                        {
                            tableLayoutPanel.GetControlFromPosition(i, j).BackColor = Color.Transparent;
                            tableLayoutPanel.GetControlFromPosition(i, j).Text = "";
                        }
                        if (tableLayoutPanel.GetControlFromPosition(i, j).Text == _secondMatch.Text)
                        {
                            tableLayoutPanel.GetControlFromPosition(i, j).BackColor = Color.Transparent;
                            tableLayoutPanel.GetControlFromPosition(i, j).Text = "";
                        }
                    }
                }
                _firstMatch = null;
                _secondMatch = null;
            }
            // Else reset two clicked labels
            else
            {
                timerIcons.Start();
            }

            // Check of victory
            if (CheckVictory())
            {
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result = MessageBox.Show("Restart?", "You win!", buttons);

                if (result == DialogResult.Yes)
                {
                    Application.Restart();
                }
                else
                {
                    this.Close();
                }
            }

            // Number of tries
            _triesNumber++;
            this.Text = $"Number of tries: {_triesNumber.ToString()}";
            if (_triesNumber >= 20)
            {
                MessageBoxButtons buttons = MessageBoxButtons.YesNo;
                DialogResult result = MessageBox.Show("Restart?", "You lose!", buttons);

                if (result == DialogResult.Yes)
                {
                    Application.Restart();
                }
                else
                {
                    this.Close();
                }
            }
        }

        private void TitleBlinking_Tick(object sender, EventArgs e)
        {
            this.Text = this.Text.Equals("Find two same cells! You have 20 tries!") ? " " : "Select two same cells! You have 20 tries!";
        }

        private void TimerIcons_Tick(object sender, EventArgs e)
        {
            timerIcons.Stop();

            // Resetting labels on tick
            _firstMatch.ForeColor = _firstMatch.BackColor;
            _secondMatch.ForeColor = _secondMatch.BackColor;
            _firstMatch = null;
            _secondMatch = null;
        }

        private bool CheckVictory()
        {
            foreach (Control control in tableLayoutPanel.Controls)
            {
                Label labelIcon = control as Label;
                if (labelIcon != null && labelIcon.ForeColor.Equals(labelIcon.BackColor))
                {
                    return false;
                }
            }
            tableLayoutPanel.BackColor = Color.Transparent;
            foreach (Control control in tableLayoutPanel.Controls)
            {
                Label labelIcon = control as Label;
                if (labelIcon != null)
                {
                    labelIcon.Text = "";
                    labelIcon.ForeColor = Color.Transparent;
                }
            }
            return true;
        }
    }
}
